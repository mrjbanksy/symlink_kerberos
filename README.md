Role Name
=========

symlink_kerberos - create symlinks from power broker Kerberos to /usr/bin where ansible is looking

Requirements
------------

This assumes kerberos utilities are at /opt/pbis/bin

Role Variables
--------------

No settable variable

Dependencies
------------

No external dependencies

Example Playbook
----------------

    - hosts: servers
      tasks:
        - name: create kerberos symlinks
          include_role:
            name: symlink_kerberos

License
-------


Author Information
------------------

Jeremy Banks (jbanks@redhat.com)
